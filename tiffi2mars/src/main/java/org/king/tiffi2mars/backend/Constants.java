package org.king.tiffi2mars.backend;

/**
 * Defines all symbolic constants
 * 
 * @author Oscar Barrios (srbarrios@gmail.com)
 */

public class Constants {

	/**
	 * Movements
	 */

	public static final char LEFT_ROTATION = 'L';
	public static final char RIGHT_ROTATION = 'R';
	public static final char MOVE = 'M';

	/**
	 * Cardinal points
	 */
	public static char NORD = 'N';
	public static char EST = 'E';
	public static char SUD = 'S';
	public static char WEST = 'W';

	/**
	 * Limits
	 */

	public static int[] X_RANGE = { 0, 9 };
	public static int[] Y_RANGE = { 0, 9 };
	public static char[] VALID_ORIENTATION = { NORD, EST, SUD, WEST };
	public static int MAX_NUM_MOVEMENTS = 50;

}
