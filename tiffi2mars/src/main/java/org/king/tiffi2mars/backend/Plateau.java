package org.king.tiffi2mars.backend;

import java.util.logging.Logger;

/**
 * Main class for the Tiffi2Mars app.
 * 
 * @author Oscar Barrios (srbarrios@gmail.com)
 */
public class Plateau {

	// GRID
	public int[] position = { 0, 0 };
	public char orientation = 'N';

	// LOGGER
	public final Logger LOGGER = Logger.getLogger("Plateau");

	public int[] getPosition() {
		LOGGER.fine("Getting Position: " + this.position);
		return this.position;
	}

	public char getOrientation() {
		LOGGER.fine("Getting Orientation: " + this.position);
		return this.orientation;
	}

	private void setPosition(int[] position) throws PlateauException {
		LOGGER.fine("Setting Position: " + position);

		if (position[0] < Constants.X_RANGE[0]
				|| position[0] > Constants.X_RANGE[1])
			throw new PlateauException("Wrong movement: Over boundaries X = "
					+ position[0], 1);

		if (position[1] < Constants.Y_RANGE[0]
				|| position[1] > Constants.Y_RANGE[1])
			throw new PlateauException("Wrong movement: Over boundaries Y = "
					+ position[1], 1);

		this.position = position;
	}

	private void setOrientation(char orientation) throws PlateauException {
		LOGGER.fine("Setting Orientation: " + orientation);
		if (orientation == Constants.EST || orientation == Constants.SUD
				|| orientation == Constants.NORD
				|| orientation == Constants.WEST) {
			this.orientation = orientation;
		} else {
			throw new PlateauException("Wrong orientation = " + orientation, 3);
		}

	}

	private void rotate_right() throws PlateauException {
		LOGGER.fine("Rotating to right...");
		switch (this.orientation) {
		case 'N':
			this.setOrientation(Constants.EST);
			break;
		case 'E':
			this.setOrientation(Constants.SUD);
			break;
		case 'S':
			this.setOrientation(Constants.WEST);
			break;
		case 'W':
			this.setOrientation(Constants.NORD);
			break;
		default:
			break;
		}
	}

	private void rotate_left() throws PlateauException {
		LOGGER.fine("Rotating to left...");
		switch (this.orientation) {
		case 'N':
			this.setOrientation(Constants.WEST);
			break;
		case 'W':
			this.setOrientation(Constants.SUD);
			break;
		case 'S':
			this.setOrientation(Constants.EST);
			break;
		case 'E':
			this.setOrientation(Constants.NORD);
			break;
		default:
			break;
		}
	}

	private void move() throws PlateauException {
		LOGGER.fine("Moving...");
		int[] new_position = this.position.clone();
		switch (this.orientation) {
		case 'N':
			new_position[1] = new_position[1] + 1;
			break;
		case 'W':
			new_position[0] = new_position[0] - 1;
			break;
		case 'S':
			new_position[1] = new_position[1] - 1;
			break;
		case 'E':
			new_position[0] = new_position[0] + 1;
			break;
		default:
			break;
		}
		this.setPosition(new_position);
	}

	private void performMovement(char movement) throws PlateauException {
		LOGGER.fine("Perform movement: " + movement);
		switch (movement) {
		case Constants.LEFT_ROTATION:
			this.rotate_left();
			break;
		case Constants.RIGHT_ROTATION:
			this.rotate_right();
			break;
		case Constants.MOVE:
			this.move();
			break;
		default:
			throw new PlateauException("Unknown movement", 2);
		}
	}

	public String moveTiffi(String landingPosition, String movementPlaned)
			throws PlateauException {
		LOGGER.fine("Checking limit of movements...");
		// Limit of movements
		if (movementPlaned.length() > Constants.MAX_NUM_MOVEMENTS)
			throw new PlateauException("Over boundary : Number of movements > "
					+ Constants.MAX_NUM_MOVEMENTS, 4);

		// Checking Landing Position
		if (landingPosition.length() > 3)
			throw new PlateauException("Over boundary : Wrong Position = "
					+ landingPosition, 5);

		LOGGER.fine("Setting Landing Position: " + landingPosition);
		// Set Landing Position
		int[] landing_position = {
				Character.getNumericValue(landingPosition.charAt(0)),
				Character.getNumericValue(landingPosition.charAt(1)) };
		this.setPosition(landing_position);
		this.setOrientation(landingPosition.charAt(2));

		LOGGER.fine("Perform movements: " + movementPlaned);
		// Perform movements
		for (char movement : movementPlaned.toCharArray()) {
			this.performMovement(movement);
		}

		int[] lastPosition = this.getPosition();
		return Integer.toString(lastPosition[0])
				+ Integer.toString(lastPosition[1]) + this.getOrientation();
	}

}
