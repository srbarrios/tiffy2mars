/**
 * 
 */
package org.king.tiffi2mars.backend;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * @author Oscar Barrios
 * 
 */
public class HappyPath {

	private Plateau plateau;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		this.plateau = new Plateau();
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		this.plateau = null;
	}

	/**
	 * Test method for
	 * {@link org.king.tiffi2mars.backend.Plateau#moveTiffi(String, String)}.
	 */
	@Test
	public final void testMoveTiffi() {
		try {
			System.out.println("Happy Paths...");
			assertEquals("HappyPath 1: ", "24E",
					this.plateau.moveTiffi("44N", "LMMMRRM"));
			assertEquals("HappyPath 2: ", "00N",
					this.plateau.moveTiffi("00N", "LLLLRRRRLLLLRRRR"));
			assertEquals("HappyPath 3: ", "39E",
					this.plateau.moveTiffi("99W", "MMMMMMMLRLRLRLRLLRLRLRRRRM"));
			assertEquals("HappyPath 4: ", "00S",
					this.plateau.moveTiffi("35S", "MRMRMRMRMRMMMLMMMM"));
		} catch (PlateauException e) {
			e.printStackTrace();
		}
	}

}
