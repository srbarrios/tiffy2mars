package component.plateau.Movements;

import static org.junit.Assert.assertEquals;

import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Logger;

import org.jbehave.core.Embeddable;
import org.jbehave.core.annotations.*;
import org.jbehave.core.configuration.*;
import org.jbehave.core.embedder.*;
import org.jbehave.core.i18n.*;
import org.jbehave.core.io.*;
import org.jbehave.core.junit.*;
import org.jbehave.core.model.*;
import org.jbehave.core.parsers.*;
import org.jbehave.core.reporters.*;
import org.jbehave.core.steps.*;
import org.jbehave.core.steps.ParameterConverters.ExamplesTableConverter;
import org.junit.Test;
import org.king.tiffi2mars.backend.Plateau;
import org.king.tiffi2mars.backend.PlateauException;

public class AcceptanceTests extends JUnitStory {

	public final Logger LOGGER = Logger.getLogger("Plateau_UnitTests");
	public Plateau plateau = null;
	public int error_code = 0;

	@Given("a new Plateau")
	public void givenANewPlateau() {
		this.plateau = new Plateau();
	}

	@When("I perform the [movements] starting on [landing_position]")
	public void whenIPerformTheMovementsindex(@Named("movements") String movementPlaned,@Named("landing_position") String landingPosition ) {
		try {
			this.plateau.moveTiffi(landingPosition, movementPlaned);
		} catch (PlateauException e) {
			//Set error_code if exist, to be evaluated by assert later
			error_code = e.getError_code();
			//e.printStackTrace();
		}
	}

	@Then("I obtain the [last_position]")
	public void thenIObtainThePositionindex(@Named("last_position") String last_position) {
		int[] expectedPosition = {
				Character.getNumericValue(last_position.charAt(0)),
				Character.getNumericValue(last_position.charAt(1))};
		char expectedOrientation = last_position.charAt(2);
		
		//Get real result
		int[] position = this.plateau.getPosition();
		char orientation = this.plateau.getOrientation();
		
		//Verify
		assertEquals("Expected Last Position", position[0], expectedPosition[0]);
		assertEquals("Expected Last Position", position[1], expectedPosition[1]);
		assertEquals("Expected Last Orientation", orientation,
				expectedOrientation);
	}

	@Then("I obtain an error_code [error_code]")
	public void thenIObtainAnError_code(@Named("error_code") String error_value) {
		assertEquals("Expected error code",	Integer.parseInt(error_value),this.error_code);
	}
	
    @Override
    public Configuration configuration() {
        Class<? extends Embeddable> embeddableClass = this.getClass();
        Properties viewResources = new Properties();
        viewResources.put("decorateNonHtml", "true");
        ParameterConverters parameterConverters = new ParameterConverters();
        ExamplesTableFactory examplesTableFactory = new ExamplesTableFactory(new LocalizedKeywords(),
                new LoadFromClasspath(embeddableClass), parameterConverters);
        parameterConverters.addConverters(new ExamplesTableConverter(examplesTableFactory));
 
        return new MostUsefulConfiguration()
                .useStoryControls(new StoryControls().doDryRun(false).doSkipScenariosAfterFailure(false))
                .useStoryLoader(new LoadFromClasspath(embeddableClass))
                .useStoryParser(new RegexStoryParser(examplesTableFactory))
                .useStoryPathResolver(new UnderscoredCamelCaseResolver())
                .useParameterConverters(parameterConverters)
                .useStoryReporterBuilder(new StoryReporterBuilder().withFormats(Format.CONSOLE));
    }
	 
	 
	 @Override
	 public List<CandidateSteps> candidateSteps() {
		 return new InstanceStepsFactory(configuration(), this).createCandidateSteps();
	 }
	 
	 @Override
	 @Test
	 public void run() throws Throwable {
		 super.run();
	 }
	 
}
